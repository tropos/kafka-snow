# Kafka Snow Project
Welcome to the Kafka Snow Project

# Features
run your own local kafka cluster on docker
extract data from snowflake to kafka
extract data from kafka to snowflake
using the rsa features for the snowflake connector
using the native python api using rsa / basic authentication to communicate with snowflake


# Project descriptions
pythonconnect
    using python to connecto to snowflake 
        using basic authentication 
        using RSA
    
kafka-connect-complex-example
    Example how to run kafka connect 
    
kafka-connect-jdbc-sf
    How to use the JDBC connecto to source snowflake
    
kafka-connect-sfconnector
    How to use the Snowflake sink connector
    How to use the source and sink together
    

kafka-confluent-docker-531
    Basic example how to run docker 531    
    
kafka-opensource-docker
    Basic example to run open source docker
    
    
# Quick and usefull commands
# Quick docker commands
docker ps -a
docker rm $(docker ps -a -q)
docker image ls -a
docker rmi 2036b53af04c
docker image ls -a
docker-compose build
docker-compose up
docker exec -it schema-registry /bin/bash
docker logs -f schema-registry

# Quick commands kafka
docker exec -it broker kafka-console-producer --broker-list localhost:9092 --topic weather
docker exec -it broker kafka-console-consumer --bootstrap-server localhost:9092 --topic sf_CUSTOMER_ADDRESS --from-beginning
docker exec broker kafka-topics --bootstrap-server broker:9092 --topic sf_CUSTOMER_ADDRESS --describe
docker exec broker kafka-topics --bootstrap-server broker:9092 --list
docker exec broker kafka-topics --bootstrap-server broker:9092 --topic orders --create --partitions 1 --replication-factor 1
docker exec broker kafka-topics --bootstrap-server broker:9092 --topic rawusers --create --partitions 1 --replication-factor 1

# Curl interesting commands
curl -s "http://localhost:8083/connectors"
curl -s "http://localhost:8083/connectors"
curl -s "http://localhost:8083/connectors/jdbc_source_snowflake_tropos_v3/status"
curl -s "http://localhost:8083/connectors/jdbc_source_snowflake_tropos_v3/config"
curl -s "http://localhost:8083/connectors/jdbc_sink_snowflake_tropos_v3/status"
curl -s "http://localhost:8083/connectors/jdbc_sink_snowflake_tropos_v3/config"
curl -s "http://localhost:8083/connectors/jdbc_sink_snowflake_tropos_v3/config/validate"



Enjoy!
Koen Vantomme 2019



