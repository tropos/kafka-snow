# Snowflake JDBC SF native 
Using kafka connect with JDBC with the JDBC-snowflake connector to produce data from Snowflake 
into Kafka topic

# Start the kafka cluster
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker image ls -a
docker rmi xxx
docker-compose build
docker-compose up

# Explain kakfa connect JDBC
The kakfa connect JDBC api is used but to communicate with snowflake we need to exten it with the snowflake-jdbc-connector-3.9.2.jar
Thats why we define it in the seperate dockerfile
# Explain : Dockerfile :: docker build image - add the snowflake jdbc connector and create a plugin 
RUN mkdir -p /usr/share/java/plugins
RUN mkdir -p /usr/share/java/kafka-connect-jdbc
RUN curl -sSL "https://repo1.maven.org/maven2/net/snowflake/snowflake-jdbc/3.9.2/snowflake-jdbc-3.9.2.jar" \
-o /usr/share/java/kafka-connect-jdbc/snowflake-jdbc-connector-3.9.2.jar


# Example :customise the jdbc url for snowflake with connections parameters
jdbc:snowflake://<account_name>.snowflakecomputing.com/?<connection_params>
jdbc:snowflake://tropos.eu-central-1.snowflakecomputing.com:443/?warehouse=explorer&db=sandbox&schema=explorer",
jdbc:snowflake://xy12345.snowflakecomputing.com/?user=peter&warehouse=mywh&db=mydb&schema=public
jdbc:snowflake://tropos.eu-central-1.snowflakecomputing.com:443/?warehouse=explorer&db=sandbox&schema=explorer"


# sf source
curl -X POST http://localhost:8083/connectors -H "Content-Type: application/json" -d '{
        "name": "jdbc_source_snowflake_tropos_vbase",
        "config": {
                "connector.class": "io.confluent.connect.jdbc.JdbcSourceConnector",
                "connection.url": "jdbc:snowflake://tropos.eu-central-1.snowflakecomputing.com:443/?warehouse=explorer&db=sandbox&schema=explorer",
                "connection.user": "kvantomme@tropos.io",
                "connection.password": "Tropos2019&",
                "topic.prefix": "sf_",
                "mode":"bulk",
                "table.whitelist" : "SNOWFLAKE_SAMPLE_DATA.TPCDS_SF10TCL.CUSTOMER_ADDRESS"
                }
        }'

# SF Sink is complicated - gives problems
    - Use the snowflake connector, special written for this
curl -X POST http://localhost:8083/connectors -H "Content-Type: application/json" -d '{
        "name": "jdbc_sink_snowflake_tropos_v3",
        "config": {
                "connector.class": "io.confluent.connect.jdbc.JdbcSinkConnector",
                "connection.url": "jdbc:snowflake://tropos.eu-central-1.snowflakecomputing.com:443/?warehouse=explorer&db=sandbox&schema=explorer",
                "connection.user": "kvantomme@tropos.io",
                "connection.password": "Tropos2019&",
                "topics" :"sf_CUSTOMER_ADDRESS",
                "auto.create": "true",
                "auto.evolve": "true",
                "insert.mode": "insert"
                }
        }'
        
# example: standalone command top run in docker
$ ./bin/connect-standalone etc/schema-registry/connect-avro-standalone.properties etc/kafka-connect-jdbc/sink-quickstart-sqlite.properties

# docker schema registry
docker exec -it schema-registry /bin/bash
cat /etc/schema-registry/connect-avro-standalone.properties
=> bootstrap.servers=localhost:9092
bin/schema-registry-start ./etc/schema-registry/schema-registry.properties


# avro producer docker
# not working
docker exec -it f574e5da6deb /bin/bash
docker exec -it schema-registry kafka-avro-console-producer \
 --broker-list broker:29092 --topic orders_v1 \
 --property value.schema='{"type":"record","name":"myrecord","fields":[{"name":"id","type":"int"},{"name":"product", "type": "string"}, {"name":"quantity", "type": "int"}, {"name":"price",
 "type": "float"}]}'

# avro producer command to run in docker
./usr/bin/kafka-avro-console-producer \
 --broker-list broker:29092 --topic orders_v1 \
 --property value.schema='{"type":"record","name":"myrecord","fields":[{"name":"id","type":"int"},{"name":"product", "type": "string"}, {"name":"quantity", "type": "int"}, {"name":"price", "type": "float"}]}'

{"id": 9991, "product": "koen", "quantity": 1000, "price": 1500}
{"id": 9992, "product": "joris", "quantity": 1000, "price": 2500}
{"id": 9994, "product": "joman", "quantity": 3000, "price": 3500}

# avro consumer  
./usr/bin/kafka-avro-console-consumer --bootstrap-server broker:29092 --topic orders_v1 --from-beginning
  

# kafka connect sink
curl -X POST http://localhost:8083/connectors -H "Content-Type: application/json" -d '{
        "name": "jdbc_sink_snowflake_avro_tropos_v3",
        "config": {
                "connector.class": "io.confluent.connect.jdbc.JdbcSinkConnector",
                "connection.url": "jdbc:snowflake://tropos.eu-central-1.snowflakecomputing.com:443/?warehouse=explorer&db=sandbox&schema=explorer",
                "connection.user": "kvantomme@tropos.io",
                "connection.password": "Tropos2019&",
                "topics" :"orders_v1",
                "auto.create": "true",
                "auto.evolve": "true",
                "insert.mode": "insert"
                }
        }'

curl -X POST http://localhost:8083/connectors -H "Content-Type: application/json" -d '{
        "name": "jdbc_sink_snowflake_avro_tropos_v3",
        "config": {
                "connector.class": "io.confluent.connect.jdbc.JdbcSinkConnector",
                "connection.url": "jdbc:snowflake://tropos.eu-central-1.snowflakecomputing.com:443/?warehouse=explorer&db=sandbox&schema=explorer",
                "connection.user": "kvantomme@tropos.io",
                "connection.password": "Tropos2019&",
                "topics": "orders_v1",
                "value.converter": "com.snowflake.kafka.connector.records.SnowflakeJsonConverter",
                "key.converter": "org.apache.kafka.connect.storage.StringConverter",
                "schemas.enable": "false"
                }
        }'

