#!/usr/bin/env python
import snowflake.connector

print("Connect to snowflake")

# Gets the version
ctx = snowflake.connector.connect(
    user='snowkafka',
    password='Tropos2019',
    account='tropos.eu-central-1'
)

cs = ctx.cursor()
try:
    cs.execute("SELECT current_version()")
    one_row = cs.fetchone()
    print(one_row[0])
finally:
    cs.close()
ctx.close()