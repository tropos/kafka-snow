# https://docs.snowflake.net/manuals/user-guide/python-connector-example.html#using-key-pair-authentication
import snowflake.connector
import os
import logging
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.asymmetric import dsa
from cryptography.hazmat.primitives import serialization

v_path = "/Users/koenvantomme/Documents/kafka/kafka-snow/pythonconnect/rsa_key6/"
v_user='kvantomme@tropos.io'
v_account='tropos.eu-central-1'
v_database = 'sandbox'
v_schema = 'explorer'
v_warehehouse = 'explorer'
v_log_path='/Users/koenvantomme/Documents/kafka/kafka-snow/pythonconnect/log/'

#logging.basicConfig(
#    filename='/Users/koenvantomme/Documents/kafka/kafka-snow/pythonconnect/log'
#             '/sf_python_connector_1.log',
#    level=logging.DEBUG)

for logger_name in ['snowflake','botocore']:
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    ch = logging.FileHandler(v_log_path + 'sf_python_connector_2.log')
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(logging.Formatter('%(asctime)s - %(threadName)s %(filename)s:%(lineno)d '
                                      '- %(funcName)s() - %(levelname)s - %(message)s'))
    logger.addHandler(ch)

with open(v_path + "rsa_key.p8", "rb") as key:
    p_key= serialization.load_pem_private_key(
        key.read(),
        password=os.environ['PRIVATE_KEY_PASSPHRASE'].encode(),
        backend=default_backend()
    )

pkb = p_key.private_bytes(
    encoding=serialization.Encoding.DER,
    format=serialization.PrivateFormat.PKCS8,
    encryption_algorithm=serialization.NoEncryption())

ctx = snowflake.connector.connect(
    user=v_user,
    account=v_account,
    private_key=pkb,
    warehouse=v_warehehouse,
    database=v_database,
    schema=v_schema
    )

cs = ctx.cursor()

try:
    cs.execute("SELECT current_version()")
    one_row = cs.fetchone()
    print(one_row[0])
finally:
    cs.close()
ctx.close()
