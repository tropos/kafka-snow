from typing import Dict, Optional
import snowflake.connector


v_user='kvantomme@tropos.io'
v_account='tropos.eu-central-1'
# v_path = '/Users/koenvantomme/Documents/kafka/kafka-snow/pythonconnect/rsa2/rsa_key.priv'
v_warehouse='explorer',
v_database='sandbox',
v_schema='explorer'
v_password='Tropos2019&'
v_role='explorer'


class SnowflakeParameters:
    def get_connection_parameters(self) -> Dict[str, str]:
        return {
            "user": v_user,
            "password": v_password,
            "account": v_account,
            "database": v_database,
            "warehouse": v_warehouse,
            "role": v_role,
        }


init_query = "SELECT current_version()"
connection_parameters = SnowflakeParameters().get_connection_parameters()
conn = snowflake.connector.connect(**connection_parameters)


cs = conn.cursor()
try:
    cs.execute("SELECT current_version()")
    one_row = cs.fetchone()
    print(one_row[0])
finally:
    cs.close()



