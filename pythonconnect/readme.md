# Python connect to snowflake
Python exampkle to connect to snowflake using the rsa authentication

# generate tokens
openssl genrsa -out rsa_key.pem 2048
openssl genrsa 2048 | openssl pkcs8 -topk8 -inform PEM -out rsa_key.p8
openssl rsa -in rsa_key.p8 -pubout -out rsa_key.pub
openssl rsa -in rsa_key.p8 -out rsa_key.priv
 
# define in snowflake
alter user jsmith set rsa_public_key='MIIBIjANBgkqh...';

# set environment variable
export PRIVATE_KEY_PASSPHRASE=tropos

# run python to snwoflake basic authentication
python3 connect_sf_simple.py

# run python to snwoflake basic authentication class
python3 connect_sf_simple_class.py

# run python to snowflake rsa
python3 connect_sf_rsa.py


 