# SNOWFLAKE DB

Scenario 1
----------
** generate data 
** produce to topic
** consume to snowflake

Scenario 2
-----------
** Read data from SF and store on topic
** Read from topic and ingest in SF
** define data as three tables

Scenario 3
----------
** Produce all data from schema to topic
** Consume all data form topic to snowflake

Scenario 4
----------
** Check avro / json / schema registry
** Check CDC
** Check incrementals

Scenario 4
----------
** Check mysql
** Check debezium

Scenario 5
----------
** Deploy on AWS
** Deploy Kubernetes

Scenario 6
----------
** Kstreams
** Flink
** KSQL
** Confluent Examples
** Schema registry
** Rest API


