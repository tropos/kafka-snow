# Kafka connect using the snowflake connector using source and sink
Using the snowflake connector we can sink data from kafka into snowflake
For source we use kakfa connect jdbc extended with kakfa connector jdbc
For sink we use a custom snowflake connector written by snowflake

# Generate tokens
The communication from kafka to snowflake using the snowflake connector will make use of rsa 
(source)
openssl genrsa -out rsa_key.pem 2048
openssl genrsa 2048 | openssl pkcs8 -topk8 -inform PEM -out rsa_key.p8
openssl rsa -in rsa_key.p8 -pubout -out rsa_key.pub
openssl rsa -in rsa_key.p8 -out rsa_key.priv
# define in snowflake
alter user jsmith set rsa_public_key='MIIBIjANBgkqh...';


#
# SCENARIO TWO : Read data from SNOWFLAKE AND WRITE BACK TO SNOWFLAKE USING KAFKA AS REAL TIME STORAGE LAYER
#

# Snowflake prepare 
# clean up the snowflake environment
# drop tables on kafka target
drop table "SANDBOX"."KAFKA_TARGET"."KAFKACUSTOMER";
drop table "SANDBOX"."KAFKA_TARGET"."KAFKADEMOGRAPHICS";
drop table "SANDBOX"."KAFKA_TARGET"."KAFKASALES";
# clean stages , only delete the relevant stages !
use database sandbox
use schema kafka_target
show stages
drop stage SNOWFLAKE_KAFKA_CONNECTOR_KAFKASNOWFLAKEINGESTKAFKAPOC_STAGE_KAFKACUSTOMER
# create schema
create schema kafka_orig;
create schema kakfa_target;
# Prepare the testdata 
create table "SANDBOX"."KAFKA_ORIG"."CATALOG_SALES" as select * from "SNOWFLAKE_SAMPLE_DATA"."TPCDS_SF10TCL"."CATALOG_SALES" LIMIT 10000;
create table "SANDBOX"."KAFKA_ORIG"."CUSTOMER" as select * from "SNOWFLAKE_SAMPLE_DATA"."TPCDS_SF10TCL"."CUSTOMER" LIMIT 10000;
create table "SANDBOX"."KAFKA_ORIG"."CUSTOMER_DEMOGRAPHICS" as select * from "SNOWFLAKE_SAMPLE_DATA"."TPCDS_SF10TCL"."CUSTOMER_DEMOGRAPHICS" LIMIT 10000;


# Start the kafka cluster
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker image ls -a
docker rmi xxx
docker-compose build
docker-compose up


# Prepare the jdbc source using kafka connect from snowflake   
curl -X POST http://localhost:8083/connectors -H "Content-Type: application/json" -d '{
        "name": "jdbc_source_snowflake_tropos",
        "config": {
                "connector.class": "io.confluent.connect.jdbc.JdbcSourceConnector",
                "connection.url": "jdbc:snowflake://tropos.eu-central-1.snowflakecomputing.com:443/?warehouse=explorer",
                "connection.user": "kvantomme@tropos.io",
                "connection.password": "Tropos2019&",
                "topic.prefix": "poc",
                "mode":"bulk",
                "max.interval": 1000,
                "iterations": 10000000,
                "tasks.max": "8",
                "table.whitelist" : "SANDBOX.KAFKA_ORIG.CUSTOMER_DEMOGRAPHICS,SANDBOX.KAFKA_ORIG.CUSTOMER,SANDBOX.KAFKA_ORIG.CATALOG_SALES",
                "key.converter":"org.apache.kafka.connect.storage.StringConverter",
                "value.converter": "org.apache.kafka.connect.json.JsonConverter",
                "value.converter.schemas.enable": "false"
                }
        }'

# Validate data is written to snowflake    
# list
docker exec broker kafka-topics --bootstrap-server broker:9092 --list    
# consumer
docker exec -it broker kafka-console-consumer --bootstrap-server localhost:9092 --topic pocCUSTOMER --from-beginning
docker exec -it broker kafka-console-consumer --bootstrap-server localhost:9092 --topic pocCATALOG_SALES --from-beginning

# Configure the sink
docker exec -it connect curl -X POST http://localhost:8083/connectors -H "Content-Type: application/json" -d @tmp/snowflake-connector-kafkapoc.json

#overview
select * from "SANDBOX"."KAFKA_TARGET"."KAFKACUSTOMER";
select * from "SANDBOX"."KAFKA_TARGET"."KAFKADEMOGRAPHICS";
select * from "SANDBOX"."KAFKA_TARGET"."KAFKASALES";
  
  
  
#
# SCENARIO ONE : Sink data to snowflake using the snowflake sink connector and using a data 
# producer for generating data
#

# datagenerator simple
$ seq 5 | confluent local produce topic1

# datagenerator advanced
use the datagenerators offered in confluent hub
In the docker file (Dockerfile):
ENV CONNECT_PLUGIN_PATH="/usr/share/java,/usr/share/confluent-hub-components"
RUN confluent-hub install --no-prompt confluentinc/kafka-connect-datagen:0.1.0

# Run the data generator command
docker exec -it connect curl -X POST -H "Content-Type: application/json" -H 'Accept:application/json' -d @/tmp/datagen-users.json http://localhost:8083/connectors


# Content of the datagen-users.json
# we have copied this into /tmp of container connect in the Dockerfile
{
  "name": "datagen-users",
  "config": {
    "connector.class": "io.confluent.kafka.connect.datagen.DatagenConnector",
    "kafka.topic": "topic2",
    "quickstart": "users",
    "key.converter": "org.apache.kafka.connect.storage.StringConverter",
    "value.converter": "org.apache.kafka.connect.json.JsonConverter",
    "value.converter.schemas.enable": "false",
    "max.interval": 1000,
    "iterations": 10000000,
    "tasks.max": "1"
  }
}

Notes:
This is a predefined user schema from confluent, very interesting


# Example of snowflake-connector definition
Remark the .p8 definition in private key

{
  "name":"kafkasnowflakeingestusergenerator",
  "config":{
    "connector.class":"com.snowflake.kafka.connector.SnowflakeSinkConnector",
    "tasks.max":"8",
    "topics":"users",
    "snowflake.topic2table.map": "users:rawusersv4",
    "offset.flush.interval.ms":"60000",
    "offset.flush.timeout.ms":"10000",
    "buffer.flush.time":"60",
    "buffer.count.records":"100",
    "buffer.count.records":"100",
    "buffer.size.bytes":"65536",
    "snowflake.url.name":"tropos.eu-central-1.snowflakecomputing.com",
    "snowflake.user.name":"kvantomme@tropos.io",
    "snowflake.private.key":"MIIE6TAbBgkqhkiG9w0BBQMwDgQIBPfnaHWljzsCAggABIIEyAEvmSsJohITAvTjsTmdCZ3pLN1Wnpr/48ijznPhVOU7zXVJ2LajJrM2RbuKpSJbg9D2xiUcho880lCuiHNWB9qAOS5ffx9FkwEmIAjvY11N6Zo2S/GmTRx6ipHBTycW8XQYSXvJKjZaKF4M6vKBIunJw8KUaND1cVAwFRbY0YQH/66TLNM1MM5nHXsMpqZKt2AD3hCP7zt/F6sJvjoE/sMFkzYr4qejudYPSUgFMBHwEaY/ierPPHJb3LDd0wHKIrIMprcOINLgPY3q6QAFF/a5gFM7xOIRxcRP549RNX90PqYcsIL/XOR65Hp8fCqCgG9mn1IfFut6odAzwpTmZiR/pRzC0dboWRUlKvoCbsWmuv+dY3UK/5kUY7Tm+BxJUu80Juf6C2KbUzRFvbBVM8jleZDJCvKNOTbJ3M9EYvq3wC6/o7tbcapP4sWPajs3BpK7gGPawgmPS9mMKfFFrYdFHCY7vKXMoycFn0z+kmYbigT4vh1tksLs7egI1Gp/9gmkKg2C5HLeQUGtOd3heZuMOhGXzvE5L1v5ystA5I0+VBGMEReadrgRoL2BYrYGb1i6sbvA0KxT76/m4D/TUyN8MVCH7PNWqSdj7Zk4ODL79/k6J9k/GEjPaD2ZBZgkKCk6cBK1RlM2RaKjkuxhv5sUcQXn9kkWNJyq9cDKjk334tgGdvt7vpjjC/9/AfZTE4yDEg2USTgHVf8A3KFrE51nlWn9hFQr2aX+1Nu36QawHflokMKMV9HSdfLgPoeaQYCTQPEAM4eHts/D/wiRxTy9qV/tu/dQA5ySsE+XP7yynNA53LymVOLY2Kaij80t1H2+5rXfwhVsrpuwAP++/43L/jhc5KtOi+nHHnK7HFX1ZP3rK1u7mj8CQxcaKz3+UyUV2g79gSLZ0jq4hDAEmCaGVE+dcmZIE2PvRKo3CBQu5PfZrbqAOaLwF6sicHlRb/StyqNeu2kApGBcqNMEvElZq+4wG+czf3TxldUt0RSZBX7CifDCnF96noB5LEUUlQkc8oOGfr1vT257KsdgblGB5U5gsM5McHq5/BY+9R6MwZyxvN7OEFFzMXaLmzoscbZ4IA2ypdWvUEVAl+KB9JWoGqIdvc6PivhFWKSCuqPzehNq+PCSjnHJ9Y+5GNHIkyCkQ0LJdrCqUUfP/IYDRRlV2Jy8KoEEndoJZahZYQhdMUZCtPaSRgmc/UCji70I3vTJvAwp73aWUD8D/mVV4qO/LTpTTpxs2JfLZyJfXzGMD36hpplYk1OTeX8Flrpp7paHdaMhJigfAojmJgdj5ZKroD2TEw3O4dDHM4AaZibMrtFYgdCO08pgK7KEKAa4rQfbamiFIHXPdpSTRo0rsdsRpZ7fiXb7en62CxEbaxVYZdceu+mEoscrU2NFACv0PqixKrm/a0s602Irwy77F8rcH5JwN9FpvOxf9DVYyl+fYwd9v8l24rjxbDb++wlEjLRX8UdSR/Vnl3iJf7cHnrHSx9x2kCd2irWBgeNYbKg/WV1TwCNdg/3XkUAV7/wAANMR2c9UGjatmqfddNK1+0RUxwGeTXhUuby7KoqdRHH4hQBS/1WetR5Ird+nWUUf6OvCwjKx0XGMBNMXBRlEp0LjVmGTd5pGYw==",
    "snowflake.private.key.passphrase":"tropos",
    "snowflake.database.name":"sandbox",
    "snowflake.schema.name":"explorer",
    "key.converter":"org.apache.kafka.connect.storage.StringConverter",
    "value.converter":"com.snowflake.kafka.connector.records.SnowflakeJsonConverter",
    "value.converter.schema.registry.url":"http://schema-registry:8081"
  }
}


# Run the sink connector
docker exec -it connect curl -X POST http://localhost:8083/connectors -H "Content-Type: 
application/json" -d @tmp/snowflake-connector-user.json

# Verify in snowflake
select count(*) from "SANDBOX"."EXPLORER"."RAWUSERSV4"
select * from "SANDBOX"."EXPLORER"."RAWUSERSV4"

# Error, problem with connector 5.0
Reported to snowflake support




}
